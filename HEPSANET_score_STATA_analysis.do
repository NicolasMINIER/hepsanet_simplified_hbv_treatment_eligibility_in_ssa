        *************************************************************
        *                                                           *
        *                        HEPSANET 2022                      *
        *                                                           *
        *   Evaluation of patient eligibility for antiviral         *
        *       treatment against Hepatitis B Virus (HBV) in the    *
        *       subsaharian area, without access to all examination *
        *       tools required by WHO & EASL criteria.              *
        *                                                           *
        *   Authors: MINIER Nicolas, SHIMAKAWA Yusuke               *
        *                                                           *
        *   Last edited: 2023-11-26                                 *
        *                                                           *
        *************************************************************


********************************************************************************

    clear all
    cd
    set autotabgraphs on

* IMPORT CLEAN DATABASE
    use "HEPSANET_database_2022.dta", clear

* Library
    // ssc install diagt
    // ssc install kappaetc
    // ssc install mim


********************************************************************************
*                           DESCRIPTION OF THE COHORT                          *
********************************************************************************

*****************************COMPARING THE DATASETS*****************************

* Sex
    tab sex dataset, col expected chi2 exact

* Age
    //hist age, by(dataset)         // Distributions considered normal enough
    sdtest age, by(dataset)         // Variances considered unequal
    ranksum age, by(dataset)
    tabstat age, stats(mean sd p25 p50 p75) by(dataset)
    
    tab agecat dataset, column expected chi2 exact

* BMI
    //hist bmi, by(dataset)          // Distributions considered normal
    sdtest bmi, by(dataset)          // Variances considered unequal
    ranksum bmi, by(dataset)
    tabstat bmi, stats(mean sd p25 p50 p75) by(dataset)
    
    tab bmi30 dataset, column expected chi2 exact

* Family history
    tab familyhx dataset, column expected chi2 exact

* OH abuse
    tab ohabuse dataset, column expected chi2 exact

**********

* Jaundice
    tab jaundice dataset, column expected chi2 exact
    
* Ascites
    tab ascites dataset, column expected chi2 exact
    
* Hepatic encephalopathy
    * Nobody with hepatic encephalopathy in the database
    
* Variceal Bleeding
    tab varicbleed dataset, column expected chi2 exact

**********

* HBeAg
    tab hbeag dataset, column expected chi2 exact

* Viral Load
    //hist hbv_vl, by(dataset)       // Distributions not considered normal
    ranksum hbv_vl, by(dataset)
    tabstat hbv_vl, stats(mean sd p25 p50 p75) by(dataset)
    tabstat hbv_vl_log, stats(mean sd p25 p50 p75) by(dataset)

**********

* ALT
    //hist lnalt, by(dataset)       // Distributions considered normal
    sdtest lnalt, by(dataset)       // Variances considered unequal
    ranksum lnalt, by(dataset)
    tabstat alt, stats(mean sd p25 p50 p75) by(dataset)

* AST 
    //hist lnast, by(dataset)         // Distributions considered normal
    sdtest lnast, by(dataset)     // Variances considered unequal
    ranksum lnast, by(dataset)
    tabstat ast, stats(mean sd p25 p50 p75) by(dataset)

* GGT
    //hist lnggt if(ggt<200), by(dataset) // Distribution considered normal
    sdtest lnggt, by(dataset)             // Variances considered unequal
    ranksum lnggt, by(dataset)
    tabstat ggt, stats(mean sd p25 p50 p75) by(dataset)

* Bilirubin
    //hist lnbil if(bilir<5), by(dataset)  // Distribution considered normal
    sdtest lnbil, by(dataset)              // Variances considered unequal
    ranksum lnbil, by(dataset)
    tabstat bilir, stats(mean sd p25 p50 p75) by(dataset)

* Platelets
    //hist lnplat, by(dataset)     // Distributions considered normal
    sdtest lnplat, by(dataset)     // Variances considered unequal
    ranksum lnplat, by(dataset)
    tabstat plat, stats(mean sd p25 p50 p75) by(dataset)

* INR
    //hist inr if(inr<5), by(dataset) // Distr. not considered normal
    //ranksum inr, by(dataset)
    tabstat inr, by(dataset) stats(mean sd p25 p50 p75)
    * Too few INR to even compare.

* APRI
    //hist lnapri if(apri<5), by(dataset) // Distributions considered normal
    sdtest lnapri, by(dataset)            // Variances considered unequal
    ranksum lnapri, by(dataset)
    tabstat apri, stats(mean sd p25 p50 p75) by(dataset)

* FIB-4
    //hist lnfib4 if(lnfib4<5), by(dataset) // Distributions considered normal
    sdtest lnfib4, by(dataset)            // Variances considered unequal
    ranksum lnfib4, by(dataset)
    tabstat fib4, stats(mean sd p25 p50 p75) by(dataset)
**********

* Liver stiffness (TE)
    //hist te_kpa if(te_reliable==1), by(dataset)
    sdtest te_kpa if(te_reliable==1), by(dataset)
    ranksum te_kpa if(te_reliable==1), by(dataset)
    tabstat te_kpa if(te_reliable==1), stats(mean sd p25 p50 p75) by(dataset)
    
    tab te_cat dataset, column expected chi2 exact
    
    tab metavir_cat dataset, column expected chi2 exact
    
    tab fib_cat dataset, column expected chi2 exact

**********

* EASL 2017
    tab reco_easl dataset, column expected chi2 exact

* WHO 2015
    tab reco_who2015 dataset, column expected chi2 exact
	
* TREAT-B
    tab reco_treatb dataset, column expected chi2 exact
    
    
    
    
************COMPARING THE "BIG REGIONS" (West SSA and East+South SSA)***********

* Sex
    tab sex region, col expected chi2 exact

* Age
    //hist age, by(region)         // Distributions considered normal enough
    sdtest age, by(region)         // Variances considered unequal
    ranksum age, by(region)
    tabstat age, stats(mean sd p25 p50 p75) by(region)
    
    tab agecat region, column expected chi2 exact

* BMI
    //hist bmi, by(region)          // Distributions considered normal
    sdtest bmi, by(region)          // Variances considered unequal
    ranksum bmi, by(region)
    tabstat bmi, stats(mean sd p25 p50 p75) by(region)
    
    tab bmi30 region, column expected chi2 exact

* Family history
    tab familyhx region, column expected chi2 exact

* OH abuse
    tab ohabuse region, column expected chi2 exact

**********

* Jaundice
    tab jaundice region, column expected chi2 exact
    
* Ascites
    tab ascites region, column expected chi2 exact
    
* Hepatic encephalopathy
    * Nobody with hepatic encephalopathy in the database
    
* Variceal Bleeding
    tab varicbleed region, column expected chi2 exact

**********

* HBeAg
    tab hbeag region, column expected chi2 exact

* Viral Load
    //hist hbv_vl, by(region)       // Distributions not considered normal
    ranksum hbv_vl, by(region)
    tabstat hbv_vl, stats(mean sd p25 p50 p75) by(region)
    tabstat hbv_vl_log, stats(mean sd p25 p50 p75) by(region)

**********

* ALT
    //hist lnalt, by(region)       // Distributions considered normal
    sdtest lnalt, by(region)       // Variances considered unequal
    ranksum lnalt, by(region)
    tabstat alt, stats(mean sd p25 p50 p75) by(region)

* AST 
    //hist lnast, by(region)         // Distributions considered normal
    sdtest lnast, by(region)     // Variances considered unequal
    ranksum lnast, by(region)
    tabstat ast, stats(mean sd p25 p50 p75) by(region)

* GGT
    //hist lnggt if(ggt<200), by(region) // Distribution considered normal
    sdtest lnggt, by(region)             // Variances considered unequal
    ranksum lnggt, by(region)
    tabstat ggt, stats(mean sd p25 p50 p75) by(region)

* Bilirubin
    //hist lnbil if(bilir<5), by(region)  // Distribution considered normal
    sdtest lnbil, by(region)              // Variances considered unequal
    ranksum lnbil, by(region)
    tabstat bilir, stats(mean sd p25 p50 p75) by(region)

* Platelets
    //hist lnplat, by(region)     // Distributions considered normal
    sdtest lnplat, by(region)     // Variances considered unequal
    ranksum lnplat, by(region)
    tabstat plat, stats(mean sd p25 p50 p75) by(region)

* INR
    //hist inr if(inr<5), by(region) // Distr. not considered normal
    //ranksum inr, by(region)
    tabstat inr, by(region) stats(mean sd p25 p50 p75)
    * Too few INR to even compare.

* APRI
    //hist lnapri if(apri<5), by(region) // Distributions considered normal
    sdtest lnapri, by(region)            // Variances considered unequal
    ranksum lnapri, by(region)
    tabstat apri, stats(mean sd p25 p50 p75) by(region)

* FIB-4
    //hist lnfib4 if(lnfib4<5), by(region) // Distributions considered normal
    sdtest lnfib4, by(region)            // Variances considered unequal
    ranksum lnfib4, by(region)
    tabstat fib4, stats(mean sd p25 p50 p75) by(region)
    
**********

* Liver stiffness (TE)
    //hist te_kpa if(te_reliable==1), by(region)
    sdtest te_kpa if(te_reliable==1), by(region)
    ranksum te_kpa if(te_reliable==1), by(region)
    tabstat te_kpa if(te_reliable==1), stats(mean sd p25 p50 p75) by(region)
    
    tab te_cat region, column expected chi2 exact
    
    tab metavir_cat region, column expected chi2 exact
    
    tab fib_cat region, column expected chi2 exact

**********

* EASL 2017
    tab reco_easl region, column expected chi2 exact

* WHO 2015
    tab reco_who2015 region, column expected chi2 exact
	
* TREAT-B
    tab reco_treatb region, column expected chi2 exact





********************************************************************************
*                       UNI- AND MULTI-VARIABLE ANALYSIS                       *
********************************************************************************

******************************Univariable analysis******************************

* SEX
    tab sex reco_easl, column expected chi2 exact
    logistic reco_easl sex

* AGE
    //hist age, by(reco_easl)  // Distributions considered normal
    sdtest age, by(reco_easl)  // Variances considered equal
    ranksum age, by(reco_easl)
    tabstat age, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl age
    
    * If we use agecat
    tab agecat reco_easl, column expected chi2 exact
    logistic reco_easl agecat

* HBeAg
    tab hbeag reco_easl, column expected chi2 exact
    logistic reco_easl hbeag
    
* ALT
    //hist lnalt, by(reco_easl)  // Distributions considered normal
    sdtest lnalt, by(reco_easl)  // Variances considered unequal
    ranksum lnalt, by(reco_easl)
    tabstat lnalt, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl lnalt

* AST 
    //hist lnast, by(reco_easl)  // Distributions considered normal
    sdtest lnast, by(reco_easl)  // Variances considered unequal
    ranksum lnast, by(reco_easl)
    tabstat lnast, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl lnast
    
* Platelets
    //hist lnplat, by(reco_easl) // Distributions considered normal
    sdtest lnplat, by(reco_easl) // Variances considered unequal
    ranksum lnplat, by(reco_easl)
    tabstat lnplat, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl lnplat

* GGT
    //hist lnggt, by(reco_easl)  // Distr. normal
    sdtest lnggt, by(reco_easl)            // Variances unequal
    ranksum lnggt, by(reco_easl)
    tabstat lnggt, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl lnggt
    
* INR
    //hist inr, by(reco_easl)  // Distr. not normal
    ranksum inr, by(reco_easl)
    tabstat inr, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl inr
    
* bilirubin
    //hist lnbil, by(reco_easl)  // Distr. normal
    sdtest lnbil, by(reco_easl)            // Variances unequal
    ranksum lnbil, by(reco_easl)
    tabstat lnbil, stat(mean sd p50 p25 p75) by(reco_easl)
    logistic reco_easl lnbil
    
    
    
 
********************************************************************************
*                      DEVELOPMENT OF THE SCORING SYSTEMS                      *
********************************************************************************

* See MINIER et al. (2024), and SULLIVAN et al. (2004) for details.


*************************************TIER 0*************************************
* Multivariable regression
    stepwise, pr(.01) pe(.005) forward: ///
        logistic reco_easl sex age if(dataset==1 & ///
                                      ((familyhx==0 | familyhx==.) & ///
                                       (jaundice==. | jaundice==0)))
    
    logit reco_easl sex if(dataset==1 & ///
                           ((familyhx==0 | familyhx==.) & ///
                            (jaundice==. | jaundice==0)))

    gen reco_tier0 = sex
    replace reco_tier0 = 1 if(jaundice == 1)
    replace reco_tier0 = 1 if(familyhx == 1)
    diagt reco_easl reco_tier0 if(dataset==1)


*************************************TIER 1*************************************
* Same as TIER0.




*******************************MULTIPLE IMPUTATION******************************
* Since we have missing data for some predictor candidates (GGT, HBeAg), we will
*   perform multiple imputation to derive our logistic model without losing the
*   information contained in participants with missing data.

* Check for differences between people with HBeAg available or not
    tab site hbeag, m

    gen hashbeag = 0
    replace hashbeag=1 if(hbeag!=.)
    
    gen hasggt=0
    replace hasggt=1 if(ggt!=.)

    gen hasboth=0
    replace hasboth=1 if(hasggt==1 & hashbeag==1)
    
    tab sex hashbeag, col expected chi2 exact
    
    //hist age, by(hashbeag)       // Distributions considered normal
    sdtest age, by(hashbeag)       // Variances considered unequal
    ranksum age, by(hashbeag)
    tabstat age, stats(mean sd p25 p50 p75) by(hashbeag)
	
    tab hospcom hashbeag, col expected chi2 exact
    
    //hist hbv_vl_log, by(hashbeag)       // Distributions considered normal
    sdtest hbv_vl_log, by(hashbeag)       // Variances considered unequal
    ranksum hbv_vl_log, by(hashbeag)
    tabstat hbv_vl_log, stats(mean sd p25 p50 p75) by(hashbeag)
    
    tab fib_cat hashbeag, col expected chi2 exact
    
    //hist lnalt, by(hashbeag)       // Distributions considered normal
    sdtest lnalt, by(hashbeag)       // Variances considered equal
    ttest lnalt, by(hashbeag)
    tabstat alt, stats(mean sd p25 p50 p75) by(hashbeag)
    
    //hist lnast, by(hashbeag)       // Distributions considered normal
    sdtest lnast, by(hashbeag)       // Variances considered unequal
    ranksum lnast, by(hashbeag)
    tabstat ast, stats(mean sd p25 p50 p75) by(hashbeag)
    
    //hist lnplat, by(hashbeag)       // Distributions considered normal
    sdtest lnplat, by(hashbeag)       // Variances considered unequal
    ranksum lnplat, by(hashbeag)
    tabstat plat, stats(mean sd p25 p50 p75) by(hashbeag)
    
    //hist lnggt, by(hashbeag)       // Distributions considered normal
    sdtest lnggt, by(hashbeag)       // Variances considered unequal
    ranksum lnggt, by(hashbeag)
    tabstat ggt, stats(mean sd p25 p50 p75) by(hashbeag)
    

* We perform imputation on HBeAg and lnGGT according to the factors found to be
*   associated with their presence/absence.
    mi set flong
    mi register imputed sex region hospcom lnast lnplat hbeag lnggt 
    mi impute chained (logit) hbeag (regress) lnggt = sex region hospcom lnast lnplat, add(20) rseed(123) force
    gen _mi = _mi_id
    gen _mj = _mi_m




*************************************TIER 2*************************************
* Predictor selection
xi: mim: stepwise, pr(0.01) pe(0.005) forward: ///
    logit reco_easl sex age lnplat lnalt lnast lnggt ///
        if(dataset==1 & nsigns==0 & (familyhx==0 | familyhx==.))

* Logit coefficients
logit reco_easl lnplat lnalt lnast ///
    if(dataset==1 & nsigns==0 & (familyhx==0 | familyhx==.))

* Points
    gen points_tier2=0
    replace points_tier2=points_tier2+1 if(plat<150 & plat>=100)
    replace points_tier2=points_tier2+2 if(plat<100)
    replace points_tier2=points_tier2+1 if(alt>=40 & alt<80)
    replace points_tier2=points_tier2+2 if(alt>=80)
    replace points_tier2=points_tier2+1 if(ast>=40 & ast<80)
    replace points_tier2=points_tier2+2 if(ast>=80)
    replace points_tier2=6 if(familyhx==1)
    replace points_tier2=6 if(nsigns>0)

    
    

*************************************TIER 3*************************************
* Predictor selection		
xi: mim: stepwise, pr(0.001) pe(0.0005) forward: ///
    logit reco_easl sex age (lnplat lnalt lnast) lnggt hbeag ///
        if(dataset==1 & nsigns==0 & (familyhx==0 | familyhx==.))

* Logit coefficients
logit reco_easl hbeag lnplat lnalt lnast ///
    if(dataset==1 & nsigns==0 & (familyhx==0 | familyhx==.) & ///
       _mi_m==0)

* Imputation assuming worst case scenario (Eligible people are considered
*   HBeAg-negative, and ineligible people are HBeAg-positive)
    gen hbeag_imputed = hbeag
    replace hbeag_imputed = 0 if(hbeag == . & reco_easl == 1)
    replace hbeag_imputed = 1 if(hbeag == . & reco_easl == 0)
    
    gen points_tier3_imputed=0
    replace points_tier3_imputed=points_tier2+2*hbeag_imputed
    replace points_tier3_imputed=8 if(familyhx==1)
    replace points_tier3_imputed=8 if(nsigns>0)

* Just for the record
    * Points on complete cases
    gen points_tier3=0 if(hbeag!=.)
    replace points_tier3=points_tier2+2*hbeag
    replace points_tier3=8 if(familyhx==1)
    replace points_tier3=8 if(nsigns>0)




********************************************************************************
*                                    CLEAN-UP                                  *
********************************************************************************

* No longer need multiple imputations, we discard them to avoid specifying
*   "& _mi_mi==0" everywhere and risking an error

drop if(_mi_m!=0)

drop _mi_m _mi_id _mi_miss




********************************************************************************
*                             DIAGNOSIS PERFORMANCE                            *
********************************************************************************

*************************************TIER 2*************************************

* Threshold
    roctab reco_easl points_tier2 if(dataset==1), table detail
    * max(AUROC) reach for ≥2

    gen reco_tier2=0
    replace reco_tier2=1 if(points_tier2>=2)
    diagt reco_easl reco_tier2 if(dataset==1)
    diagt reco_easl reco_tier2 if(dataset==2)


    
    
*********************************TIER 3 (HBeAg)*********************************

* Threshold
    roctab reco_easl points_tier3 if(dataset==1), table detail
    * max(AUROC) reach for ≥2

    * On complete cases
    gen reco_tier3=0 if(points_tier3!=.)
    replace reco_tier3=1 if(points_tier3>=2 & points_tier3!=.)
    
    diagt reco_easl reco_tier3 if(dataset==1)
    diagt reco_easl reco_tier3 if(dataset==2)
    
    * On worst case scenario single imputation
    gen reco_tier3_imputed=0
    replace reco_tier3_imputed=1 if(points_tier3_imputed>=2)
    
    diagt reco_easl reco_tier3_imputed if(dataset==1)
    diagt reco_easl reco_tier3_imputed if(dataset==2)
    
    
    
    
**************Diagnosis performance of index and comparative tests**************

diagt reco_easl reco_tier0 if(dataset==1)
diagt reco_easl reco_tier2 if(dataset==1)
diagt reco_easl reco_tier3 if(dataset==1)
diagt reco_easl reco_tier3_imputed if(dataset==1)
diagt reco_easl reco_who2015 if(dataset==1)
diagt reco_easl reco_treatb if(dataset==1)

diagt reco_easl reco_tier0 if(dataset==2)
diagt reco_easl reco_tier2 if(dataset==2)
diagt reco_easl reco_tier3 if(dataset==2)
diagt reco_easl reco_tier3_imputed if(dataset==2)
diagt reco_easl reco_who2015 if(dataset==2)
diagt reco_easl reco_treatb if(dataset==2)




********************************SUBGROUP ANALYSIS*******************************

* Sensibility of TIER2 simple scoring system to various parameters 
diagt reco_easl reco_tier2 if(dataset==1)
roccomp reco_easl reco_tier2 if(dataset==1), by(sex)
    diagt reco_easl reco_tier2 if(sex==0 & dataset==1)
    diagt reco_easl reco_tier2 if(sex==1 & dataset==1)
roccomp reco_easl reco_tier2 if(dataset==1), by(agecat)
    diagt reco_easl reco_tier2 if(agecat==0 & dataset==1)
    diagt reco_easl reco_tier2 if(agecat==1 & dataset==1)
roccomp reco_easl reco_tier2 if(dataset==1), by(ohabuse)
    diagt reco_easl reco_tier2 if(ohabuse==0 & dataset==1)
    diagt reco_easl reco_tier2 if(ohabuse==1 & dataset==1)
roccomp reco_easl reco_tier2 if(dataset==1), by(region)
    diagt reco_easl reco_tier2 if(region==1 & dataset==1)
    diagt reco_easl reco_tier2 if(region==2 & dataset==1)
roccomp reco_easl reco_tier2 if(dataset==1), by(hospcom)
    diagt reco_easl reco_tier2 if(hospcom==0 & dataset==1)
    diagt reco_easl reco_tier2 if(hospcom==1 & dataset==1)
// roccomp reco_easl reco_tier2 if(dataset==1), by(signs_yn)
    // diagt reco_easl reco_tier2 if(signs_yn==0 & dataset==1)
    // diagt reco_easl reco_tier2 if(signs_yn==1 & dataset==1)
    
    
* Sensibility of TIER2 simple scoring system to various parameters 
diagt reco_easl reco_tier2 if(dataset==2)
roccomp reco_easl reco_tier2 if(dataset==2), by(sex)
    diagt reco_easl reco_tier2 if(sex==0 & dataset==2)
    diagt reco_easl reco_tier2 if(sex==1 & dataset==2)
roccomp reco_easl reco_tier2 if(dataset==2), by(agecat)
    diagt reco_easl reco_tier2 if(agecat==0 & dataset==2)
    diagt reco_easl reco_tier2 if(agecat==1 & dataset==2)
roccomp reco_easl reco_tier2 if(dataset==2), by(ohabuse)
    diagt reco_easl reco_tier2 if(ohabuse==0 & dataset==2)
    diagt reco_easl reco_tier2 if(ohabuse==1 & dataset==2)
roccomp reco_easl reco_tier2 if(dataset==2), by(region)
    diagt reco_easl reco_tier2 if(region==1 & dataset==2)
    diagt reco_easl reco_tier2 if(region==2 & dataset==2)
roccomp reco_easl reco_tier2 if(dataset==2), by(hospcom)
    diagt reco_easl reco_tier2 if(hospcom==0 & dataset==2)
    diagt reco_easl reco_tier2 if(hospcom==1 & dataset==2)
// roccomp reco_easl reco_tier2 if(dataset==1), by(signs_yn)
    // diagt reco_easl reco_tier2 if(signs_yn==0 & dataset==1)
    // diagt reco_easl reco_tier2 if(signs_yn==1 & dataset==1)
    
    
* Sensibility of TIER3 simple scoring system to various parameters
diagt reco_easl reco_tier3 if(dataset==1)
roccomp reco_easl reco_tier3 if(dataset==1), by(sex)
    diagt reco_easl reco_tier3 if(sex==0 & dataset==1)
    diagt reco_easl reco_tier3 if(sex==1 & dataset==1)
roccomp reco_easl reco_tier3 if(dataset==1), by(agecat)
    diagt reco_easl reco_tier3 if(agecat==0 & dataset==1)
    diagt reco_easl reco_tier3 if(agecat==1 & dataset==1)
roccomp reco_easl reco_tier3 if(dataset==1), by(ohabuse)
    diagt reco_easl reco_tier3 if(ohabuse==0 & dataset==1)
    diagt reco_easl reco_tier3 if(ohabuse==1 & dataset==1)
roccomp reco_easl reco_tier3 if(dataset==1), by(region)
    diagt reco_easl reco_tier3 if(region==1 & dataset==1)
    diagt reco_easl reco_tier3 if(region==2 & dataset==1)
roccomp reco_easl reco_tier3 if(dataset==1), by(hospcom)
    diagt reco_easl reco_tier3 if(hospcom==0 & dataset==1)
    diagt reco_easl reco_tier3 if(hospcom==1 & dataset==1)
// roccomp reco_easl reco_tier3 if(dataset==1), by(signs_yn)
    // diagt reco_easl reco_tier3 if(signs_yn==0 & dataset==1)
    // diagt reco_easl reco_tier3 if(signs_yn==1 & dataset==1)
    
* Sensibility of TIER3 simple scoring system to various parameters
diagt reco_easl reco_tier3 if(dataset==2)
roccomp reco_easl reco_tier3 if(dataset==2), by(sex)
    diagt reco_easl reco_tier3 if(sex==0 & dataset==2)
    diagt reco_easl reco_tier3 if(sex==1 & dataset==2)
roccomp reco_easl reco_tier3 if(dataset==2), by(agecat)
    diagt reco_easl reco_tier3 if(agecat==0 & dataset==2)
    diagt reco_easl reco_tier3 if(agecat==1 & dataset==2)
roccomp reco_easl reco_tier3 if(dataset==2), by(ohabuse)
    diagt reco_easl reco_tier3 if(ohabuse==0 & dataset==2)
    diagt reco_easl reco_tier3 if(ohabuse==1 & dataset==2)
roccomp reco_easl reco_tier3 if(dataset==2), by(region)
    diagt reco_easl reco_tier3 if(region==1 & dataset==2)
    diagt reco_easl reco_tier3 if(region==2 & dataset==2)
roccomp reco_easl reco_tier3 if(dataset==2), by(hospcom)
    diagt reco_easl reco_tier3 if(hospcom==0 & dataset==2)
    diagt reco_easl reco_tier3 if(hospcom==1 & dataset==2)
// roccomp reco_easl reco_tier3 if(dataset==1), by(signs_yn)
    // diagt reco_easl reco_tier3 if(signs_yn==0 & dataset==1)
    // diagt reco_easl reco_tier3 if(signs_yn==1 & dataset==1)




********************************************************************************
*                                ALTERNATIVE TESTS                             *
********************************************************************************

* What if we rely on APRI instead of platelet + AST + ALT ?
    gen apri_cat = round(100*apri-2.5,5)/100

* Ideal threshold
    roctab reco_easl apri_cat if(dataset==1), table detail
    * max Youden's J at APRI ≥0.40
    
* Performances
    gen reco_apri04 = 0
    replace reco_apri04 = 1 if(familyhx == 1)
    replace reco_apri04 = 1 if(nsigns >= 1)
    gen reco_apri15 = reco_apri04
    
    replace reco_apri04 = 1 if(apri >= 0.4)
    replace reco_apri15 = 1 if(apri >= 1.5)
    
    diagt reco_easl reco_apri04 if(dataset==1)
    diagt reco_easl reco_apri04 if(dataset==2)
    
    diagt reco_easl reco_apri15 if(dataset==1)
    diagt reco_easl reco_apri15 if(dataset==2)
    
    
    
    
* TREAT-B with single imputation (worst case scenario)
    gen reco_treatb_imputed = 0
    replace reco_treatb_imputed = 1 if(familyhx == 1)
    replace reco_treatb_imputed = 1 if(nsigns >= 1)
    replace reco_treatb_imputed = 1 if(hbeag_imputed==1 & alt>=20)
    replace reco_treatb_imputed = 1 if(alt>=40)
    
    diagt reco_easl reco_treatb_imputed if(dataset==1)
    diagt reco_easl reco_treatb_imputed if(dataset==2)




* What if we consider other thresholds for HEPSANET points?
    gen reco_clinic = 0
    replace reco_clinic = 1 if(familyhx == 1)
    replace reco_clinic = 1 if(nsigns >= 1)

    gen reco_hepsa21 = 0
    replace reco_hepsa21 = 1 if(points_tier2>=1)
    gen reco_hepsa22 = 0
    replace reco_hepsa22 = 1 if(points_tier2>=2)
    gen reco_hepsa23 = 0
    replace reco_hepsa23 = 1 if(points_tier2>=3)
    gen reco_hepsa24 = 0
    replace reco_hepsa24 = 1 if(points_tier2>=4)
    gen reco_hepsa25 = 0
    replace reco_hepsa25 = 1 if(points_tier2>=5)
    gen reco_hepsa26 = 0
    replace reco_hepsa26 = 1 if(points_tier2>=6)
    
    gen reco_hepsa31 = 0 if(points_tier3!=.)
    replace reco_hepsa31 = 1 if(points_tier3>=1 & points_tier3!=.)
    gen reco_hepsa32 = 0 if(points_tier3!=.)
    replace reco_hepsa32 = 1 if(points_tier3>=2 & points_tier3!=.)
    gen reco_hepsa33 = 0 if(points_tier3!=.)
    replace reco_hepsa33 = 1 if(points_tier3>=3 & points_tier3!=.)
    gen reco_hepsa34 = 0 if(points_tier3!=.)
    replace reco_hepsa34= 1 if(points_tier3>=4 & points_tier3!=.)
    gen reco_hepsa35 = 0 if(points_tier3!=.)
    replace reco_hepsa35 = 1 if(points_tier3>=5 & points_tier3!=.)
    gen reco_hepsa36 = 0 if(points_tier3!=.)
    replace reco_hepsa36 = 1 if(points_tier3>=6 & points_tier3!=.)
    gen reco_hepsa37 = 0 if(points_tier3!=.)
    replace reco_hepsa37 = 1 if(points_tier3>=7 & points_tier3!=.)
    gen reco_hepsa38 = 0 if(points_tier3!=.)
    replace reco_hepsa38 = 1 if(points_tier3>=8 & points_tier3!=.)
    
    gen reco_hepsa31_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa31_imputed = 1 if(points_tier3_imputed>=1)
    gen reco_hepsa32_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa32_imputed = 1 if(points_tier3_imputed>=2)
    gen reco_hepsa33_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa33_imputed = 1 if(points_tier3_imputed>=3)
    gen reco_hepsa34_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa34_imputed= 1 if(points_tier3_imputed>=4)
    gen reco_hepsa35_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa35_imputed = 1 if(points_tier3_imputed>=5)
    gen reco_hepsa36_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa36_imputed = 1 if(points_tier3_imputed>=6)
    gen reco_hepsa37_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa37_imputed = 1 if(points_tier3_imputed>=7)
    gen reco_hepsa38_imputed = 0 if(points_tier3_imputed!=.)
    replace reco_hepsa38_imputed = 1 if(points_tier3_imputed>=8)
    
    diagt reco_easl reco_clinic if(dataset==1)
    diagt reco_easl reco_hepsa21 if(dataset==1)
    diagt reco_easl reco_hepsa22 if(dataset==1)
    diagt reco_easl reco_hepsa23 if(dataset==1)
    diagt reco_easl reco_hepsa24 if(dataset==1)
    diagt reco_easl reco_hepsa25 if(dataset==1)
    diagt reco_easl reco_hepsa26 if(dataset==1)
    diagt reco_easl reco_hepsa31 if(dataset==1)
    diagt reco_easl reco_hepsa32 if(dataset==1)
    diagt reco_easl reco_hepsa33 if(dataset==1)
    diagt reco_easl reco_hepsa34 if(dataset==1)
    diagt reco_easl reco_hepsa35 if(dataset==1)
    diagt reco_easl reco_hepsa36 if(dataset==1)
    diagt reco_easl reco_hepsa37 if(dataset==1)
    diagt reco_easl reco_hepsa38 if(dataset==1)
    diagt reco_easl reco_hepsa31_imputed if(dataset==1)
    diagt reco_easl reco_hepsa32_imputed if(dataset==1)
    diagt reco_easl reco_hepsa33_imputed if(dataset==1)
    diagt reco_easl reco_hepsa34_imputed if(dataset==1)
    diagt reco_easl reco_hepsa35_imputed if(dataset==1)
    diagt reco_easl reco_hepsa36_imputed if(dataset==1)
    diagt reco_easl reco_hepsa37_imputed if(dataset==1)
    diagt reco_easl reco_hepsa38_imputed if(dataset==1)

    diagt reco_easl reco_clinic if(dataset==2)
    diagt reco_easl reco_hepsa21 if(dataset==2)
    diagt reco_easl reco_hepsa22 if(dataset==2)
    diagt reco_easl reco_hepsa23 if(dataset==2)
    diagt reco_easl reco_hepsa24 if(dataset==2)
    diagt reco_easl reco_hepsa25 if(dataset==2)
    diagt reco_easl reco_hepsa26 if(dataset==2)
    diagt reco_easl reco_hepsa31 if(dataset==2)
    diagt reco_easl reco_hepsa32 if(dataset==2)
    diagt reco_easl reco_hepsa33 if(dataset==2)
    diagt reco_easl reco_hepsa34 if(dataset==2)
    diagt reco_easl reco_hepsa35 if(dataset==2)
    diagt reco_easl reco_hepsa36 if(dataset==2)
    diagt reco_easl reco_hepsa37 if(dataset==2)
    diagt reco_easl reco_hepsa38 if(dataset==2)
    diagt reco_easl reco_hepsa31_imputed if(dataset==2)
    diagt reco_easl reco_hepsa32_imputed if(dataset==2)
    diagt reco_easl reco_hepsa33_imputed if(dataset==2)
    diagt reco_easl reco_hepsa34_imputed if(dataset==2)
    diagt reco_easl reco_hepsa35_imputed if(dataset==2)
    diagt reco_easl reco_hepsa36_imputed if(dataset==2)
    diagt reco_easl reco_hepsa37_imputed if(dataset==2)
    diagt reco_easl reco_hepsa38_imputed if(dataset==2)




