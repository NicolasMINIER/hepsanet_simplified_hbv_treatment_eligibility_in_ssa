## Related publication
The code made available here is related to the following article:

[MINIER <i>et al.</i>, 2024](https://doi.org/10.1016/S2468-1253(23)00449-1). <b><i>Development and evaluation of a simple treatment eligibility score (HEPSANET) to decentralise hepatitis B care in Africa: a cross-sectional study</i></b>. The Lancet Gastroenterology and hepatology. Available online 15 February 2024.

DOI: [10.1016/S2468-1253(23)00449-1](https://doi.org/10.1016/S2468-1253(23)00449-1)


## Purpose of the analysis

This repository contains code for the development of a simplified scoring system designed to assess eligibility for antiviral treatment for sub-Saharan African individuals chronically infected with Hepatitis B virus (HBV) in low-resource contexts, without the need for either liver biopsy, transient elastography, or PCR.

The simplified scoring system developed here is largely based on the methodology described by [Sullivan <i>et al.</i> (2004)](https://doi.org/10.1002/sim.1742), and is similar in concept to that developed by [Shimakawa <i>et al.</i> (2018)](https://doi.org/10.1016/j.jhep.2018.05.024) which shared the same goal but failed to be widely applied in practice, notably due to its reliance on HBeAg, which is not easily available everywhere.

Data analysis was performed on [STATA 17](https://www.stata.com/)


## HEPSANET consortium
The Hepatitis B in Africa Collaborative Network (HEPSANET) is a consortium of hospitals and research centers across 8 sub-Saharan African countries, in collaboration with several European research institutes.
HEPSANET assembled a ~4000 participants large cohort of HBV chronically infected individuals coming from 13 cohorts representing 8 sub-Saharan African countries. This database serves in several projects of the consortium, such as this one. A general description of the cohort and scientfic agenda of HEPSANET has been made available by [Riches <i>et al.</i> (2023)](10.1017/S095026882300050X).
